import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;


public class gui {
	JFrame frame= new JFrame();
	JButton[] b= new JButton[40];
	int looper=0;
	JTextField result;
	String arrayVal="{11,21,32,41,52,62,72,82}";
	StringBuilder tempString = new StringBuilder("00011111");
	StringBuilder tempString2 = new StringBuilder(arrayVal);
	int[] individulaBit=new int[40];
	int no=0;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new gui().show();
	}
void show(){
	frame.setLayout(null);
	frame.setBounds(100,100,500,320);
	frame.setForeground(Color.WHITE);
	frame.setBackground(Color.WHITE);
	frame.getContentPane().setForeground(Color.WHITE);
	frame.getContentPane().setBackground(Color.WHITE);
	frame.setResizable(false);
	frame.setIconImage(new ImageIcon(getClass().getResource("icon.jpg")).getImage());
	result = new JTextField("Array");
	result.setForeground(Color.black);
	result.setBounds(200,20,290,20);
	frame.getContentPane().add(result);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	JMenuBar menubar = new JMenuBar();

    JMenu file = new JMenu("Edit");
    file.setMnemonic(KeyEvent.VK_F);
    
    JMenuItem clearMenu = new JMenuItem("Clear");
    clearMenu.setMnemonic(KeyEvent.VK_E);
    clearMenu.setToolTipText("Exit application");
    clearMenu.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
           for(int temp=0;temp<40;temp++)
           {
        	   b[temp].setBackground(Color.WHITE);
        	   individulaBit[temp]=0;
           }
           putResult();
        }
    });
    file.add(clearMenu);
    
    JMenuItem eMenuItem = new JMenuItem("Exit");
    eMenuItem.setMnemonic(KeyEvent.VK_E);
    eMenuItem.setToolTipText("Exit application");
    eMenuItem.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
            System.exit(0);
        }
    });
    file.add(eMenuItem);
    
    menubar.add(file);

    JMenu abt = new JMenu("About");
    file.setMnemonic(KeyEvent.VK_F);
    
    menubar.add(abt);
    
    frame.setJMenuBar(menubar);
	 
	JEditorPane details= new JEditorPane();
	details.setBounds(200,50,290,220);
	details.setContentType("text/html");
	String p = getClass().getResource("logo.jpg").toString();
	details.setText("<html><table><tr><td align=\"center\"><b>Custom Character Generator V_1.0</b></td></tr>"
			+ "<tr><td align=\"center\"><img src=\""+p+"\" width=\"100\" height=\"60\" \"></td></tr>"
			+ "<tr><td><i>Embedded Lab,TIFAC Core</i></td></tr>"
			+ "<tr><td>National Institute of Science and Technology</td></tr>"
			+ "<tr><td>Palur Hills, Berhampur,Odisha</td></tr>"
			+ "<tr><td>Blog :  <a href=\"http://www.weargenius.blogspot.in\" style=\"text-decoration:none\">www.weargenius.blogspot.in</a></td></tr>"
			+ "</table><html>");
	details.setEditable(false);
	frame.getContentPane().add(details);
	int yLoc =10;
	int xloc=10;
	for(looper=0;looper<40;looper++)
	{
		b[looper]= new JButton();
		b[looper].setForeground(Color.WHITE);
		b[looper].setBackground(Color.WHITE);
		if(looper%5==0)
		{
			yLoc=yLoc+25;
			xloc=0;
		}
		individulaBit[looper]=0;
		b[looper].setBounds(10+xloc*25,yLoc,20,20);
		b[looper].addMouseListener(new MouseAdapter() {
			int tempVar=looper;
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				Color x = b[tempVar].getBackground();
				if(x==Color.WHITE)
				{
					b[tempVar].setBackground(Color.BLACK);
					individulaBit[tempVar]=1;
				}
				else
				{
					b[tempVar].setBackground(Color.WHITE);
					individulaBit[tempVar]=0;
				}
				//Result Calculatio
				putResult();
			}
		});
		frame.getContentPane().add(b[looper]);
		xloc++;
	}
	frame.setVisible(true);
}
protected void putResult() {
	// TODO Auto-generated method stub
	int x3=1;
	for(int x1=0;x1<40;x1=x1+5){
		tempString.setCharAt(3,(char)(48+individulaBit[x1]));
		tempString.setCharAt(4,(char)(48+individulaBit[x1+1]));
		tempString.setCharAt(5,(char)(48+individulaBit[x1+2]));
		tempString.setCharAt(6,(char)(48+individulaBit[x1+3]));
		tempString.setCharAt(7,(char)(48+individulaBit[x1+4]));
		no=Integer.parseInt(tempString.toString(),2);
		String ting=""+no;
		if(no<9)
		{
			tempString2.setCharAt(x3+1,ting.charAt(0));
			tempString2.setCharAt(x3,' ');
		}
		else
		{
			tempString2.setCharAt(x3,ting.charAt(0));
			tempString2.setCharAt(x3+1,ting.charAt(1));
		}
		System.out.println(no);
		x3=x3+3;
	}
	result.setText("int custom[8]="+tempString2.toString()+";");
}
}
